import { Component, OnInit } from '@angular/core';

// As with the other services you've used, you need to import and inject the
// service before you can use it:
import { FormBuilder} from '@angular/forms';

// imort the CartService from the cart.service.ts file
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  items;
  checkoutForm; // will store the form model

  constructor(
    // injecting the CartService so that the CartComponent can use it
    private cartService : CartService,

    // inject the FormBuilder Service
    private formBuilder : FormBuilder,
  ) {
    // set the checkoutForm property with a form model
    this.checkoutForm = this.formBuilder.group({
      name: '',
      address:''
    });
  }

  ngOnInit(){
    this.items = this.cartService.getItems();
  }

  onSubmit(customerData){
    // process checkout data here
    this.items = this.cartService.clearCart();
    this.checkoutForm.reset();

    // can view this message in the browser
    console.warn('Your order has been submitted', customerData);
  }

}
